const Data = [
    {
        "id": 4,
        "countryId": 1,
        "name": "National ID Details",
        "entity": "Drivers",
        "createdBy": 1,
        "createdDate": "2021-08-18T06:52:16.443",
        "lastModifiedBy": null,
        "lastModifiedDate": "2021-08-18T06:52:16.443",
        "country": null,
        "formAttributes": [
            {
                "id": 2,
                "formId": 4,
                "inputType": "Text",
                "inputLabel": "National ID",
                "defaultValue": null,
                "fieldMapping": "field1",
                "createdBy": 1,
                "createdDate": "2021-08-18T06:52:16.443",
                "lastModifiedBy": null,
                "lastModifiedDate": "2021-08-18T06:52:16.443",
                "formAttributeValidations": [
                    {
                        "id": 2,
                        "formAttributeId": 2,
                        "name": "Required",
                        "value": "",
                        "createdBy": 1,
                        "createdDate": "2021-08-18T12:07:46.57",
                        "lastModifiedBy": null,
                        "lastModifiedDate": null
                    },
                    {
                        "id": 5,
                        "formAttributeId": 2,
                        "name": "MaxLength",
                        "value": "20",
                        "createdBy": 1,
                        "createdDate": "2021-08-19T09:29:44.33",
                        "lastModifiedBy": null,
                        "lastModifiedDate": null
                    },
                    {
                        "id": 6,
                        "formAttributeId": 2,
                        "name": "MinLength",
                        "value": "3",
                        "createdBy": 1,
                        "createdDate": "2021-08-19T09:29:44.33",
                        "lastModifiedBy": null,
                        "lastModifiedDate": null
                    }
                ],
                "formAttributeValues": [
                    {
                        "id": 1,
                        "formAttributeId": 2,
                        "type": "Text",
                        "value": "1",
                        "tableName": null,
                        "columnKey": null,
                        "createdBy": 1,
                        "columnValue": null,
                        "createdDate": "2021-08-18T06:52:16.443",
                        "lastModifiedBy": 1,
                        "lastModifiedDate": "2021-08-18T06:52:16.443"
                    }
                ]
            },
            {
                "id": 8,
                "formId": 4,
                "inputType": "Checkbox",
                "inputLabel": "Status",
                "defaultValue": null,
                "fieldMapping": "field4",
                "createdBy": 1,
                "createdDate": "2021-08-25T06:55:13.6",
                "lastModifiedBy": null,
                "lastModifiedDate": "2021-08-25T06:55:13.6",
                "formAttributeValidations": [],
                "formAttributeValues": [
                    {
                        "id": 4,
                        "formAttributeId": 8,
                        "type": "Open",
                        "value": "1",
                        "tableName": null,
                        "columnKey": null,
                        "createdBy": 1,
                        "columnValue": null,
                        "createdDate": "2021-08-25T07:30:02.747",
                        "lastModifiedBy": null,
                        "lastModifiedDate": "2021-08-25T07:30:02.747"
                    },
                    {
                        "id": 5,
                        "formAttributeId": 8,
                        "type": "In progress",
                        "value": "2",
                        "tableName": null,
                        "columnKey": null,
                        "createdBy": 1,
                        "columnValue": null,
                        "createdDate": "2021-08-25T07:30:02.747",
                        "lastModifiedBy": null,
                        "lastModifiedDate": "2021-08-25T07:30:02.747"
                    },
                    {
                        "id": 7,
                        "formAttributeId": 8,
                        "type": "Completed",
                        "value": "3",
                        "tableName": null,
                        "columnKey": null,
                        "createdBy": 1,
                        "columnValue": null,
                        "createdDate": "2021-08-25T07:30:02.747",
                        "lastModifiedBy": null,
                        "lastModifiedDate": "2021-08-25T07:30:02.747"
                    }
                ]
            },
            {
                "id": 3,
                "formId": 4,
                "inputType": "DateTime",
                "inputLabel": "National ID Expiry Date",
                "defaultValue": "Todays Date",
                "fieldMapping": "field2",
                "createdBy": 1,
                "createdDate": "2021-08-18T06:52:16.443",
                "lastModifiedBy": null,
                "lastModifiedDate": "2021-08-18T06:52:16.443",
                "formAttributeValidations": [],
                "formAttributeValues": []
            },
            {
                "id": 6,
                "formId": 4,
                "inputType": "Radio",
                "inputLabel": "Gender",
                "defaultValue": "Male",
                "fieldMapping": "field3",
                "createdBy": 1,
                "createdDate": "2021-08-25T06:55:13.6",
                "lastModifiedBy": null,
                "lastModifiedDate": "2021-08-25T06:55:13.6",
                "formAttributeValidations": [],
                "formAttributeValues": [
                    {
                        "id": 2,
                        "formAttributeId": 6,
                        "type": "Male",
                        "value": "1",
                        "tableName": null,
                        "columnKey": null,
                        "createdBy": 1,
                        "columnValue": null,
                        "createdDate": "2021-08-25T07:30:02.747",
                        "lastModifiedBy": null,
                        "lastModifiedDate": "2021-08-25T07:30:02.747"
                    },
                    {
                        "id": 3,
                        "formAttributeId": 6,
                        "type": "Femae",
                        "value": "2",
                        "tableName": null,
                        "columnKey": null,
                        "createdBy": 1,
                        "columnValue": null,
                        "createdDate": "2021-08-25T07:30:02.747",
                        "lastModifiedBy": null,
                        "lastModifiedDate": "2021-08-25T07:30:02.747"
                    }
                ]
            },
            {
                "id": 9,
                "formId": 4,
                "inputType": "Dropdown",
                "inputLabel": "Country",
                "defaultValue": null,
                "fieldMapping": "field5",
                "createdBy": 1,
                "createdDate": "2021-08-25T06:55:13.6",
                "lastModifiedBy": null,
                "lastModifiedDate": "2021-08-25T06:55:13.6",
                "formAttributeValidations": [],
                "formAttributeValues": [
                    {
                        "id": 30,
                        "formAttributeId": 9,
                        "type": "Countries",
                        "value": "1",
                        "tableName": "Countries",
                        "columnKey": "ID",
                        "createdBy": 1,
                        "columnValue": "Name",
                        "createdDate": "2021-08-25T07:35:59.167",
                        "lastModifiedBy": null,
                        "lastModifiedDate": "2021-08-25T07:35:59.167"
                    }
                ]
            },
            {
                "id": 10,
                "formId": 4,
                "inputType": "Date",
                "inputLabel": "Expiry Date",
                "defaultValue": null,
                "fieldMapping": "field6",
                "createdBy": 1,
                "createdDate": "2021-08-25T06:55:13.6",
                "lastModifiedBy": null,
                "lastModifiedDate": "2021-08-25T06:55:13.6",
                "formAttributeValidations": [],
                "formAttributeValues": []
            },
            {
                "id": 11,
                "formId": 4,
                "inputType": "File",
                "inputLabel": "National Id Photo",
                "defaultValue": null,
                "fieldMapping": "Filed7",
                "createdBy": 1,
                "createdDate": "2021-08-25T07:30:02.747",
                "lastModifiedBy": null,
                "lastModifiedDate": "2021-08-25T07:30:02.747",
                "formAttributeValidations": [],
                "formAttributeValues": []
            }
        ]
    }
]

export default Data