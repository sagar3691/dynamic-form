import React, { useState } from "react"
import { StyleSheet, Button, TouchableOpacity, View, Text } from "react-native"
import DateTimePicker from '@react-native-community/datetimepicker';

const FormDateTimePicker = (props) => {

    const {
        inputType: type,
        inputLabel: label,
        defaultValue,
        formAttributeValues: [{ type: displayName, value } = {}] = [],
        formAttributeValidations: [{ name: validName, value: validValue } = {}] = [],
    } = props.data

    const [date, setDate] = useState(new Date(1598051730000));
    const [mode, setMode] = useState('time');
    const [show, setShow] = useState(false);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };


    return (
        <View style={styles.container}>
            <Text style={styles.label}>{label}</Text>
            <View style={{ flexDirection: "row" }}>
                <Button
                    onPress={showDatepicker}
                    title="Select Date"
                    color="#C4C5F4"
                    accessibilityLabel="Learn more about this purple button" />

                <Button
                    onPress={showTimepicker}
                    title="Select Time"
                    color="#C4C534"
                    accessibilityLabel="Learn more about this purple button" />
            </View>
            {
                show && <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                />
            }
        </View>

    )
}

export default FormDateTimePicker


const styles = StyleSheet.create({

    container: {
        // backgroundColor: "red"
        paddingTop: 5,
        paddingBottom: 5
    },
    label: {
        fontSize: 12,
        paddingTop: 5,
        paddingBottom: 5
    },
})

