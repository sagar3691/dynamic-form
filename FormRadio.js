import React, { useState } from "react"
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"

const FormRadio = (props) => {

    const {
        inputType: type,
        inputLabel: label,
        defaultValue,
        formAttributeValues = [],
        formAttributeValidations: [{ name: validName, value: validValue } = {}] = [],
    } = props.data

    const [error, setError] = useState(new Error(""))

    return (
        <View style={styles.container}>
            <Text style={styles.label}>{label}</Text>
            {
                formAttributeValues.map(({ type: displayName, value } = {}) => {
                    return (
                        <TouchableOpacity style={{ flexDirection: "row" }}>
                            <Image style={{ width: 30, height: 30, marginRight: 10 }} source={require("./ic_checked.png")} />
                            <Text>{displayName}</Text>
                        </TouchableOpacity>
                    )
                })
            }
            <Text style={styles.error}>{error.message ? error.message : ""}</Text>
        </View>

    )
}

export default FormRadio

const styles = StyleSheet.create({

    container: {
        // backgroundColor: "red"
        paddingTop: 5,
        paddingBottom: 5
    },
    label: {
        fontSize: 12,
        paddingTop: 5,
        paddingBottom: 5
    },
})