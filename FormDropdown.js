import React from "react"
import { StyleSheet, Text, View } from "react-native"
import ModalDropdown from 'react-native-modal-dropdown';
import Countries from "./Countries";

const FormDropdown = (props) => {

    const {
        inputType: type,
        inputLabel: label,
        defaultValue,
        formAttributeValues: [{ type: displayName, value } = {}] = [],
        formAttributeValidations: [{ name: validName, value: validValue } = {}] = [],
    } = props.data

    const fetchData = async () => {

        Promise.resolve(Countries)
            .then(response => {
                console.log(response)
            })
            .catch(error => console.log(error))
    }

    const renderRow = ({ id, name }, index, isSelected) => {

        return <Text key={id}>{{ name }}</Text>
    }

    return (
        <View style={styles.container}>
            <Text style={styles.label}>{label}</Text>
            <ModalDropdown style={styles.label} renderRow={renderRow} options={Countries} />
        </View>
    )
}

export default FormDropdown

const styles = StyleSheet.create({

    container: {
        // backgroundColor: "red"
        paddingTop: 5,
        paddingBottom: 5
    },
    label: {
        fontSize: 12,
        paddingTop: 5,
        paddingBottom: 5
    }
})