import { EventEmitter } from "eventemitter3"
import React, { Fragment, useReducer, useState } from "react"
import { StyleSheet, Text, TextInput, View } from "react-native"

const reducer = (state, action) => {

    switch (action.type) {
        case "text":
            return { data: state.data }
    }
}

const FormInput = (props) => {

    const {
        inputType: type,
        inputLabel: label,
        defaultValue,
        fieldMapping: key,
        formAttributeValues: [{ type: displayName, value } = {}] = [],
        formAttributeValidations: [{ name: validName, value: validValue } = {}] = [],
    } = props.data

    const initialState = { data: "" }

    const [state, dispatch] = useReducer(reducer, initialState)
    const [error, setError] = useState(new Error(""))


    const onChangeText = (event) => {

        // check for errors -> set Error
        // set value in KEY

        // new CustomEvent()

        if (event.length > 3) {
            console.log("Greater than 3")
            dispatch({ type: "text" })
        }
        else {
            console.log("LESS THAN 3")
        }


        // setError(new Error("Hello"))
    }

    const fireEvent = () => {

        // dispatchEvent()

        // new EventEmitter().emit("onData", {
        //     detail: "SAGAR HERE"
        // })


        // this.dispatchEvent()

        // this.dispatchEvent(new Event("Data",))

    }

    return (
        <View style={styles.container}>
            <Text style={styles.label}>{label}</Text>
            <TextInput style={styles.input} type={type} value={defaultValue} onChangeText={onChangeText} onBlur={fireEvent} />
            <Text style={styles.error}>{error.message ? error.message : ""}</Text>
        </View>

    )
}

export default FormInput

const styles = StyleSheet.create({

    container: {
        // backgroundColor: "red"
        paddingTop: 5,
        paddingBottom: 5
    },
    label: {
        fontSize: 12,
        paddingTop: 5,
        paddingBottom: 5
    },
    input: {
        borderColor: "#CECECE",
        borderWidth: 1,
    },
    error: {
        fontSize: 10,
        color: "red"
    }
})