import React, { Fragment } from "react"
import { StyleSheet, Text, View } from "react-native"
import FormCheckbox from "./FormCheckBox"
import FormTimePicker from "./FormTimePicker"
import FormDateTimePicker from "./FormDateTimePicker"
import FormFilePicker from "./FormFilePicker"
import FormDropdown from "./FormDropdown"
import FormInput from "./FormInput"
import FormRadio from "./FormRadio"
import FormDatePicker from "./FormDatePicker"

const FormSection = props => {

    const {
        id,
        countryId,
        name,
        entity,
        country,
        formAttributes: views = []
    } = props.data;

    const onDataReceived = (e) => {
        console.log(e.detail)
    }

    return (
        <View style={styles.container} hello={(event) => alert("DATA RECEIVED")} >
            <Text style={styles.header}>{name}</Text>
            {
                views.map(view => {
                    switch (view.inputType) {
                        case "Text":
                            return <FormInput key={view.id} data={view} />
                        case "Checkbox":
                            return <FormCheckbox key={view.id} data={view} />
                        case "DateTime":
                            return <FormDateTimePicker key={view.id} data={view} />
                        case "Radio":
                            return <FormRadio key={view.id} data={view} />
                        case "Dropdown":
                            return <FormDropdown key={view.id} data={view} />
                        case "Date":
                            return <FormDatePicker key={view.id} data={view} />
                        case "File":
                            return <FormFilePicker key={view.id} data={view} />
                        default:
                            return <Text key={view.id}>No Supporting View</Text>
                    }
                })
            }
        </View >
    )
}

export default FormSection

const styles = StyleSheet.create({

    container: {
        margin: 10,
        padding: 10,
        borderWidth: 2,
        borderColor: "#DEDEDE",
        borderRadius: 10,
    },
    header: {
        fontSize: 20,
        paddingTop: 5,
        paddingBottom: 10,
        borderBottomColor: "#777",
        borderBottomWidth: 1
    }
})
