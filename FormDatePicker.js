import React, { useState } from "react"
import { StyleSheet, Text, View, Button } from "react-native"
import DateTimePicker from '@react-native-community/datetimepicker';

const FormDatePicker = (props) => {

    const {
        inputType: type,
        inputLabel: label,
        defaultValue,
        formAttributeValues: [{ type: displayName, value } = {}] = [],
        formAttributeValidations: [{ name: validName, value: validValue } = {}] = [],
    } = props.data

    const [date, setDate] = useState(new Date(1598051730000));
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    return (
        <View style={styles.container}>
            <Text style={styles.label}>{label}</Text>
            <Button
                onPress={showDatepicker}
                title="Select Date"
                color="#C4C5F4"
                accessibilityLabel="Learn more about this purple button" />
        </View>

    )
}

export default FormDatePicker


const styles = StyleSheet.create({

    container: {
        // backgroundColor: "red"
        paddingTop: 5,
        paddingBottom: 5
    },
    label: {
        fontSize: 12,
        paddingTop: 5,
        paddingBottom: 5
    },
})

