import axios from 'axios'
import React, { Component } from 'react'
import { ActivityIndicator, Button, ScrollView, View } from 'react-native'
import { Axios } from 'react-native-axios/lib/axios'
import Data from './Data'
import DriverNetworkManger from './DriverNetworkManager'
import FormSection from './FormSection'
import NetworkManger from './NetworkManager'

class DynamicForm extends Component {

    constructor(props) {
        super(props)
    }

    state = {
        isLoading: false,
        data: []
    }

    fetchData() {

        this.setState({ isLoading: true })
        const request = new NetworkManger();
        request.get()
            .then(response => {
                console.log("RESPONSE RECEIVED")
                this.setState({ isLoading: false, data: response.data })
            })
            .catch(error => {
                console.log("ERROR")
                console.log(error);
            });

        // axios.get("http://x.api.trukker.com/api/Form/GetByCountryEntity?country=1&entity=drivers")
        //     .then(response => {
        //         this.setState({ isLoading: false, data: response.data })
        //     })
        //     .catch(error => {
        //         console.log(error);
        //     });
    }

    componentDidMount() {
        this.fetchData()
    }

    createPostBody() {


    }

    submitForm = () => {

        this.props.navigation.navigate("Profile")

        // new Promise((resolve, reject) => {
        //     setTimeout(resolve(Data), 1000)
        // })
        Promise.resolve(Data)
            .then(response => {
                this.setState({ data: response })
            })
            .catch(error => console.log(error))
    }

    render() {

        return (
            <View style={{ flex: 1 }}>
                {
                    this.state.isLoading ?
                        <ActivityIndicator style={{ flex: 9 }} size="large"></ActivityIndicator>
                        : <ScrollView style={{ flex: 9 }}>
                            {
                                this.state.data.map((d, index) => <FormSection key={d.id} data={d} />)
                            }
                        </ScrollView>
                }
                <Button
                    style={{ flex: 1 }}
                    onPress={this.submitForm}
                    title="SUBMIT"
                    color="#C47574"
                    accessibilityLabel="Submit" />
            </View>

        )
    }

}

export default DynamicForm