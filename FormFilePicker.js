import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import DocumentPicker from 'react-native-document-picker'

const FormFilePicker = (props) => {

    const {
        inputType: type,
        inputLabel: label,
        defaultValue,
        formAttributeValues: [{ type: displayName, value } = {}] = [],
        formAttributeValidations: [{ name: validName, value: validValue } = {}] = [],
    } = props.data

    const pickDocument = async () => {
        try {
            const res = await DocumentPicker.pick({ type: [DocumentPicker.types.images] })
            alert(res.uri)
            console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size,
            )
        }
        catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err
            }
        }

    }

    return (
        <View style={styles.container}>
            <Text style={styles.label}>{label}</Text>
            <Image
                style={styles.selectionText}
                onPress={pickDocument}
                source={require('./ic_folder.png')} />
        </View>
    )
}

export default FormFilePicker

const styles = StyleSheet.create({

    container: {
        // backgroundColor: "red"
        paddingTop: 5,
        paddingBottom: 5
    },
    selectionText: {
        fontSize: 12,
        paddingTop: 5,
        paddingBottom: 5
    },
})