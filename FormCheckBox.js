import React, { useState } from "react"
import { StyleSheet, Text, View } from "react-native"
import CheckBox from '@react-native-community/checkbox';

const FormCheckbox = (props) => {

    const {
        inputType: type,
        inputLabel: label,
        defaultValue,
        formAttributeValues = [],
        formAttributeValidations: [{ name: validName, value: validValue } = {}] = [],
    } = props.data

    const [checked, setChecked] = useState(false)
    const [error, setError] = useState(new Error(""))

    return (
        <View style={styles.container}>
            <Text style={styles.label}>{label}</Text>
            {
                formAttributeValues.map(({ type: displayName, value } = {}) => {
                    return (
                        <View style={{ flexDirection: "row" }}>
                            <CheckBox value={checked} onValueChange={setChecked}></CheckBox>
                            <Text style={styles.label}>{displayName}</Text>
                        </View>
                    )
                })
            }
            {/* {
                formAttributeValues.map(fav => {
                    return (
                        <View style={{ flexDirection: "row" }}>
                            <CheckBox value={checked} onValueChange={setChecked}></CheckBox>
                            <Text style={styles.label}>{fav.displayName}</Text>
                        </View>
                    )
                })
            } */}
            <Text style={styles.error}>{error.message ? error.message : ""}</Text>
        </View>

    )
}

export default FormCheckbox

const styles = StyleSheet.create({

    container: {
        paddingTop: 5,
        paddingBottom: 5,
    },
    label: {
        fontSize: 12,
        paddingTop: 5,
        paddingBottom: 5
    },
    error: {
        fontSize: 10,
        color: "red"
    }
})