import { EventEmitter } from "eventemitter3"
import React, { Fragment, useState } from "react"
import { StyleSheet, Text, TextInput, View } from "react-native"

const Profile = (props) => {

    return (
        <Text style={styles.label}>SUCCESS</Text>
    )
}

export default Profile

const styles = StyleSheet.create({

    container: {
        // backgroundColor: "red"
        paddingTop: 5,
        paddingBottom: 5
    },
    label: {
        fontSize: 12,
        paddingTop: 5,
        paddingBottom: 5
    },
    input: {
        borderColor: "#CECECE",
        borderWidth: 1,
    },
    error: {
        fontSize: 10,
        color: "red"
    }
})